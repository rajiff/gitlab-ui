import description from './accordion.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
  bootstrapComponent: 'b-collapse',
};
